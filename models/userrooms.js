'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class userrooms extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      userrooms.belongsTo(models.rooms)
      userrooms.belongsTo(models.users)
    }
  }
  userrooms.init({
    userid: DataTypes.INTEGER,
    roomid: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'userrooms',
  });
  return userrooms;
};