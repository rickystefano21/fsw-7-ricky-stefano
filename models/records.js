'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class records extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      records.belongsTo(models.rooms,{foreignKey:"roomid"})
      records.belongsTo(models.users,{foreignKey:"winner"})
    }
  }
  records.init({
    roomid: DataTypes.INTEGER,
    winner: DataTypes.INTEGER,
    p1: DataTypes.STRING,
    p2: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'records',
  });
  return records;
};