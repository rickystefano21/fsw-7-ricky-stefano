const route = require("express").Router();
const {user,room_control} = require("../controller");
const middleware = require("../middleware")

route.post("/register",user.createuser);
route.post("/login",user.login);
route.get("/user",middleware.authorization,user.getalluser )
route.post("/room", room_control.create);
route.post("/fight/:id",room_control.registerroom)

module.exports = route;