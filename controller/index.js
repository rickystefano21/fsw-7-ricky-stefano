const user = require("./users_control");
const room_control = require("./room.control");

module.exports = {
    user,
    room_control
}