const {users} = require("../models");
const Token = require("../utils/token");

class user{
static async getalluser(req,res,next){
    try {
        const userlogin = req.headers.userlogin
        const get = await users.findAll()
        res.status(200).json(get)
    } catch (error) {
        console.log(error)
    }
}
    
    static async createuser(req,res,next){
        try {
            const { username,password,role } = req.body;
            await users.create({username , password , role});
            res.status(200).json({
                msg : "Data User Berhasil dibuat , Silahkan Login"
            })
                   } catch (error) {
            console.log(error);
            res.status(401).json({
                msg : "Periksa Kembali data login anda"
            })
        }
            }

            static async login(req,res,next){
                try {
                    const {username,password} = req.body
                    const logintry = await users.findOne({
                        where:{
                            username:username
                        }
                    });
                      const pass = logintry.password;
                    if(pass!==password){
                        res.status(401).json({
                            msg:"Ada kesalahan pada input data"
                        })
                    }else{
                        const _role = logintry.role
                        const _id = logintry.id
                        const token = Token.generateToken({id:_id,username,role:_role})
                        if(token){
                            req.headers.token = token;
                            res.status(200).json({
                                msg :"berhasil"
                            })
                    
                        }
                    }
                    }
                   catch (error) {
                    console.log(error);
                    res.status(401).json({
                        msg : "Periksa Kembali data login anda"
                    })
                }
                    } 
                    
                    static async loginpage(req,res,next){
                        res.render("login")
                      }
}

module.exports = user;