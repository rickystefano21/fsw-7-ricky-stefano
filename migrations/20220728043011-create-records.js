'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('records', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      roomid: {
        type: Sequelize.INTEGER,
        References:{
          model: "rooms",
          key:"id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      winner: {
        type: Sequelize.INTEGER,
        References:{
          model: "users",
          key:"id"
        },
        onUpdate:"cascade",
        onDelete:"cascade"
      },
      p1: {
        type: Sequelize.STRING
      },
      p2: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('records');
  }
};