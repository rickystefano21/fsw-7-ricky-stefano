const jwt = require("jsonwebtoken");
const secret = "wiwiwowo";

class Token{
    static async generateToken(payload){
        try{
            const token = jwt.sign(payload,secret);
            console.log(token);
            return token
        } catch (error){
            console.log(error)
            return null
        }
    }

    static decodeToken(token){
        try{
            const payload = jwt.verify(token,secret)
            return payload
        }catch(error){
            console.log(error)
            return null
        }
    }
}

module.exports = Token