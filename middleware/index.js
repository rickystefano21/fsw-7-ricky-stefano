const Token = require("../utils/token");

class Middleware{
    static authentication(req,res,next){
        try {
            const token = req.headers.token || req.body.token;
            if(token){
                const payload = Token.decodeToken(token)
                console.log(payload," Ini payload");
                if(payload){
                    req.headers.userLogin = payload;
                    next()
                } else {
                    res.status(401).json({
                        msg : "Unauthorize"
                    })
                }
            }else{
                res.status(401).json({
                    msg : "Unauthorize"
            }) 
        }} catch (error) {
            console.log(error)
            res.status(500).json({
                msg: "ada yang error"
            })
        }
    }

    static authorization(req,res,next){
        try {
            const token = req.headers.token || req.query.token
            const payload = Token.decodeToken(token)
            const _role = payload && payload.role ? payload.role : null
            
            console.log(payload,"ini payload")
            console.log(req.headers)
            if(_role === "SA"){
                next()
            }else{
                res.status(401).json({
                    msg : "tidak ada akses"
                })
            }
        } catch (error) {
            console.log(error)
            res.status(500).json({
                msg: "Terjadi Kesalahan"
            })
        }
    }

}

module.exports = Middleware
